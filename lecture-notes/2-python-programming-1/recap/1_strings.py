# Strings and Console Output

country = ';The Netherlands,NL'

print('---')
print('--- Accessing string elements ---')
print('---')

# Strings are sequences. Each letter can be accessed
# as if it were an element in a list
print(country)
print(country[0])
print(country[-1])

# The n:m notation specifies a range
print(country[1:3])
print(country[:3])
print('\n')

# Sequences support iteration in loops
for letter in country:
    print(letter)

print('''
---
--- String concatenation and parametrization ---
---
''')
# a.k.a glueing strings together and inserting
# variable values in designated locations

# The + operator is the simplest form of string
# concatenation
country = country + ",Europe"
print(country)

# The format() method is a much more powerful mechanism for
# building parametrized strings
print("Length of string is {}".format(len(country)))
print("First letter is {}".format(country[1]))

# You can insert several values in a single string by including
# as many {} in the string as you need
position = 1
print("The {}-st letter in the string is {}".format(position + 1, country[position]))

# format() supports keyword arguments
print("The {position}-st letter in the string is {value}".format(position=position + 1, value=country[position]))

# See the documentation for more information https://docs.python.org/2/library/string.html#format-string-syntax

print('''
---
--- Casting to/from strings ---
---
''')

print("2" + "2")

# To mathematically add two strings you first need to cast (i.e. transform) them
# to integers using the built-in int() function
print(int("2") + int("2"))

# The reverse is true for integers: if you want to print them or write them to a text
# file you first need to cast them to String through str() function
print("Welcome in {}, {}, {} you happy person".format(country, "bla bla", str(1)))

print('''
---
--- String operations ---
---
''')
# Strings support a wide variety of useful operations.
# Documentation: https://docs.python.org/2/library/string.html

# Replace a character with an empty string to remove it
bogus_character = ";"
country = country.replace(bogus_character,"")

# Check whether a substring is contained by a string
print('Is "The" in {}: {}'.format(country, 'The' in country))

# The split() method chops a string at a certain character
# and returns the resulting pieces as a list
places = country.split(",")
print(places[1])

# The built-in enumerate() function returns a tuple containing
# a count and the values obtained from iterating over sequence
for index, value in enumerate(country):
    print('Letter {} is {}'.format(str(index), value))

# the built-in dir() functions lists all methods of a certain Python entity

print(dir(country))

# dir() is an extremely useful mechanism for quickly getting a sense of an entity's capabilities