# TAA4 – Spatial data analysis (Std)
Deadline Apr 7

## Learning goals
-	Perform basic data cleaning, transformation and spatial analysis with the Python programming language.
-	Design and setup a repeatable analysis workflow with Python.
-	Use built-in and third-party modules/packages and create their own functions within Python.

## Students will learn to: 
-	Utilize Python functions and scripts to analyse geospatial data.
-	Create an automatic workflow with Python.

## Prerequisite:
-	Finish chapter 7: Spatial analysis with Python

## Assignment description
For this assignment you will assume the role of a municipal/provincial/national GIS expert tasked with processing the results of an election.

### Election results in The Netherlands
In the Netherlands there are four national elections citizens can vote in:

1.	General elections (Tweede Kamerverkiezingen)
2.	Provincial elections (Provinciale statenverkiezingen) (indirectly elects the senate)
3.	Municipal elections (Gemeenteraadsverkiezingen)
4.	Water boards elections (Waterschapsverkiezingen)

Each of these is held every four years. Only the provincial and water boards elections are held on the same day. The General elections were recently held, on 17 march 2021. In this assignment we are going to take a look at this election. The complete results are published by the provinces as counts per polling station. The counts of all polling stations in a province are added together and percentages are calculated. The House of Representatives seats are subsequently assigned proportionally.

Other than the final percentages it can of course also be very interesting to see the spatial distribution of the voting results. Which areas voted left? Which areas right? Which economically liberal? Which social? Which progressive? Which conservative? Where do the nationalist parties get the most votes? Where do the environmentalist parties get their votes from? Is there a big difference in urban and rural voters? What was the approximate turnout? Etcetera.. (If you are not familiar with the political parties of the Netherlands check [this wiki page](https://en.wikipedia.org/wiki/List_of_political_parties_in_the_Netherlands)).

To make things easier for international students; here a brief generalized overview of the bigger parties:
|     Party    |     Description                        |     General Alignment    |     Economical    |     Social          |
|--------------|----------------------------------------|--------------------------|-------------------|---------------------|
|     VVD      |     Conservative-liberal party.        |     Right                |     Liberal       |     Conservative    |
|     PvdA     |     Labour Party. Social Democrats.    |     Center-Left          |     Social        |     Progressive     |
|     PVV      |     National populist party.           |     Right                |     Liberal       |     Conservative    |
|     CDA      |     Christian democratic.              |     Center-right         |     Liberal       |     Conservative    |
|     SP       |     Socialist party.                   |     Left                 |     Social        |     Progressive     |
|     GL       |     Greens.                            |     Left                 |     Social        |     Progressive     |
|     CU       |     Christian democratic.              |     Center               |     Social        |     Conservative    |
|     D66      |     Progressive-liberal party.         |     Center               |     Liberal       |     Progressive     |
|     PvdD     |     Big focus on environment.          |     Left                 |     Social        |     Progressive     |
|     SGP      |     Christian conservative.            |     Right                |     Liberal       |     Conservative    |
|     FvD      |     National populist party.           |     Right                |     Liberal       |     Conservative    |

### The assignment – Analyze data
You are asked to perform a spatial analysis on election result data (which prepared in TAA2). The end result must contain the percentages of votes for each party, on different administrative levels.

To this end you are asked to design and implement a semi-automated geospatial analysis pipeline that combines the election result data with administrative unit data to produce election results on different administrative levels.

The expected deliverables are 1) a Python analysis pipeline that calculates the election results for different administrative levels and 2) a short essay documenting a) the pipeline, b) your design and implementation choices and c) the advantages/disadvantage of your analysis pipeline. See the `Deliverables and submission` section for the full list of deliverables.

Your grade is based on the thoroughness and depth of the performed analysis and the documentation of your approach and critical reflection as captured in the essay. See the `Grading` section for a full list of the grading criteria.

### Analysis
To calculate the election results, you need to calculate the percentage that voted for each political party. You must calculate this for each of the administrative units. The task is to intersect these polling stations with the data from the administrative unit to aggregate the voting results to different administrative levels.

You are allowed to use QGIS/ArcGIS to explore the data and craft an initial design of your analysis by calculating the election results for a small area.

The final/full analysis must be implemented in Python. You are advised to use GeoPandas and/or Shapely's spatial analysis methods.

### Outcome
The outcome of the analysis is (a) geospatial dataset(s) that at least contains: 
-	polygons representing administrative units

Having as mandatory attribute the percentage voted for each political party.

## Deliverables:
-	A zip-file containing:
    - a (series of) Python script(s) that calculate election results in a(n) (semi-)automated manner (see the `Grading` section for the exact requirements)
    - a short essay documenting
        - (briefly) the data analysis pipeline i.e. how does the analysis work on a very general level (since the detailed implementation can be found in your code)
        - your code design and implementation choices i.e. why did you design the data analysis as you did
        - the advantages/disadvantages of the chosen geospatial analysis method. Does, for example, intersecting two features have any undesired side-effects (such as slivers) that one should look out for?
        - strengths and weaknesses of your pipeline.

The essay and source code should be written such that a colleague, researcher, civic hacker, etc. can easily download, execute and improve/expand it without having to contact you to ask how to run the code, which dependencies are needed, which pitfalls to look out for, etc.

## Grading criteria
You work will be graded on the following general criteria in addition to the deliverables stipulated above
-	degree and sensibility of automation - ideally the whole analysis process is automated. However, complete automation is not always feasible or wise. You should strive for a sensible balance between automation and manual work, but whatever you choose: document it thoroughly.
-	use of tools and modules - when you need to Get Things Done™ it is wise to use existing tooling as much as possible (instead of building your own). Use of third-party Python and modules/libraries/APIs is greatly encouraged.
-	code quality - Is your code readable and does it run efficiently.

### Grading rubric
Please refer to the grading rubric for the complete list of grading criteria.
